//
//  main.m
//  ProgressBar
//
//  Created by Mateusz Tylman on 16.04.2015.
//  Copyright (c) 2015 Mateusz Tylman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
