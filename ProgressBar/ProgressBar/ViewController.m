//
//  ViewController.m
//  ProgressBar
//
//  Created by Mateusz Tylman on 16.04.2015.
//  Copyright (c) 2015 Mateusz Tylman. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mySpinner;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *barWidthConstraint;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mySpinner.alpha = 0.f;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    self.barWidthConstraint.constant = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)touchStart:(id)sender {
    int y =  arc4random_uniform(4) + 1 ;
    self.mySpinner.alpha = 1.f;
     self.barWidthConstraint.constant = 0;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES  ];
    
    [self.mySpinner startAnimating];
    [self.mySpinner performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:y];
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
    [self.startButton setTitle:[NSString stringWithFormat:@"Time %ds", y] forState:UIControlStateNormal];
        [weakSelf.view layoutIfNeeded];
        [UIView animateWithDuration:y animations:^{
            weakSelf.barWidthConstraint.constant = 280;
            [weakSelf.view layoutIfNeeded];
            
        }];
    });
    [self performSelector:@selector(doAfter) withObject:self afterDelay:y];
    
}
- (void)doAfter {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}
@end
